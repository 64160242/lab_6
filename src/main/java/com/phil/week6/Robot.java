package com.phil.week6;

public class Robot {
    private String name;
    private char symbol;
    private int x;
    private int y;
    public static final int MIN_X = 0;
    public static final int MIN_Y = 0;
    public static final int MAX_X = 19;
    public static final int MAX_Y = 19;

    public Robot(String name, char symbol, int x, int y){
        this.name = name;
        this.symbol = symbol;
        this.x = x;
        this.y = y;
    }

    public Robot(String name,char symbol){  // Signature
        this(name,symbol,0,0);
    }
    
    public boolean up(){
        if(y==MIN_Y) return false;
        this.y = this.y - 1;
        return true;
    }

    public boolean up(int step){
        for(int i =0; i<step; i++){
            if(up() == false){
                return false;
            }
        }
        return true;
    }

    public boolean down(){
        if(y==MAX_Y) return false;
        this.y = this.y + 1;
        return true;
    }
    public boolean down(int step){
        for(int i =0; i<step; i++){
            if(down() == false){
                return false;
            }
        }
        return true;
    }
    public boolean left(){
        if(x==MIN_Y) return false;
        this.x = this.x - 1;
        return true;
    }
    public boolean left(int step){
        for(int i =0; i<step; i++){
            if(left() == false){
                return false;
            }
        }
        return true;
    }
    public boolean right(){
        if(x==MAX_X) return false;
        this.x = this.x + 1;
        return true;
    }
    public boolean right(int step){
        for(int i =0; i<step; i++){
            if(right() == false){
                return false;
            }
        }
        return true;
    }
    public void print(){
        System.out.println("Robot: "+ name + " X:"+ x +" Y:"+ y);
    }
    public int setName(){
        return x;
    }
    public String getName(){   //Getter Methods
        return name;
    }
    public char getSymbol(){
        return symbol;
    }
    public int getX(){
        return x;
    }
    public int getY(){
        return y; 
    }

}