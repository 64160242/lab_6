package com.phil.week6;

public class RobotApp {
    public static void main(String[] args) {
        Robot ming = new Robot("Ming",'m',1,0);
        Robot us = new Robot("Us",'u',10,10);
        ming.print();
        ming.right();
        ming.print();
        us.print();

        for(int y=Robot.MIN_Y;y<=Robot.MAX_Y;y++){
            for(int x=Robot.MIN_X;x<=Robot.MAX_X;x++){
                if(ming.getX()==x && ming.getY()==y){
                    System.out.print(ming.getSymbol());
                }else if(us.getX()==x && us.getY()==y){
                    System.out.print(us.getSymbol());
                }else{
                    System.out.print("-");
                }
            }
            System.out.println();
        }

    }
}
