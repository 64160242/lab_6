package com.phil.week6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class RobotTest {
    @Test
    public void shouldCreateRobotSuccess1(){
        Robot robot = new Robot("Robot",'R',10,11);
        assertEquals("Robot", robot.getName());
        assertEquals('R', robot.getSymbol());
        assertEquals(10, robot.getX());
        assertEquals(11, robot.getY());
    }

    @Test
    public void shouldCreateRobotSuccess2(){
        Robot robot = new Robot("Robot",'R');
        assertEquals("Robot", robot.getName());
        assertEquals('R', robot.getSymbol());
        assertEquals(0, robot.getX());
        assertEquals(0, robot.getY());
    }
    @Test
    public void  shouldDownOver(){
        Robot robot = new Robot("Robot",'R',0,Robot.MAX_Y);
        assertEquals(false, robot.down());
        assertEquals(Robot.MAX_Y, robot.getY());
    }

    @Test
    public void  shouldUpNegative(){
        Robot robot = new Robot("Robot",'R',0,Robot.MIN_Y);
        assertEquals(false, robot.up());
        assertEquals(Robot.MIN_Y, robot.getY());
    }

    @Test
    public void  shouldDownSuccess(){
        Robot robot = new Robot("Robot",'R',0,0);
        assertEquals(true, robot.down());
        assertEquals(1, robot.getY());
    }

    @Test
    public void  shouldDownNSuccess1(){
        Robot robot = new Robot("Robot",'R',10,11);
        boolean result = robot.down(5);
        assertEquals(true, result);
        assertEquals(16, robot.getY());
    }
    @Test
    public void  shouldDownNFail1(){
        Robot robot = new Robot("Robot",'R',10,11);
        boolean result = robot.down(15);
        assertEquals(false, result);
        assertEquals(19, robot.getY());
    }

    @Test
    public void  shouldUpNSuccess1(){
        Robot robot = new Robot("Robot",'R',10,11);
        boolean result = robot.up(5);
        assertEquals(true, result);
        assertEquals(6, robot.getY());
    }

    @Test
    public void  shouldUpNSuccess2(){
        Robot robot = new Robot("Robot",'R',10,11);
        boolean result = robot.up(11);
        assertEquals(true, result);
        assertEquals(0, robot.getY());
    }
    
    @Test
    public void  shouldUpNFail1(){
        Robot robot = new Robot("Robot",'R',10,11);
        boolean result = robot.up(512);
        assertEquals(false, result);
        assertEquals(0, robot.getY());
    }

    @Test
    public void  shouldUpSuccess(){
        Robot robot = new Robot("Robot",'R',0,1);
        assertEquals(true, robot.up());
        assertEquals(0, robot.getY());
    }

    @Test
    public void  shouldLeftSuccess(){
        Robot robot = new Robot("Robot",'R',1,0);
        assertEquals(true, robot.left());
        assertEquals(0, robot.getX());
    }
    @Test
    public void  shouldLeftNSuccess(){
        Robot robot = new Robot("Robot",'R',11,10);
        boolean result = robot.left(5);
        assertEquals(true, result);
        assertEquals(6, robot.getX());
    }

    @Test
    public void  shouldLeftNFail(){
        Robot robot = new Robot("Robot",'R',11,10);
        boolean result = robot.left(15);
        assertEquals(false, result);
        assertEquals(0, robot.getX());
    }

    @Test
    public void  shouldRightSuccess(){
        Robot robot = new Robot("Robot",'R',0,0);
        assertEquals(true, robot.right());
        assertEquals(1, robot.getX());
    }

    @Test
    public void  shouldRightNSuccess(){
        Robot robot = new Robot("Robot",'R',11,10);
        boolean result = robot.right(5);
        assertEquals(true, result);
        assertEquals(16, robot.getX());
    }
    
    @Test
    public void  shouldRightNFail(){
        Robot robot = new Robot("Robot",'R',11,10);
        boolean result = robot.right(15);
        assertEquals(false, result);
        assertEquals(19, robot.getX());
    }

    @Test
    public void  shouldLeftOver(){
        Robot robot = new Robot("Robot",'R',0,0);
        assertEquals(false, robot.left());
        assertEquals(Robot.MIN_X, robot.getX());
    }
    
    @Test
    public void  shouldRightOver(){
        Robot robot = new Robot("Robot",'R',Robot.MAX_X,0);
        assertEquals(false, robot.right());
        assertEquals(Robot.MAX_X, robot.getX());
    }

}